extern crate rand;
extern crate rand_distr;

use std::fmt::Debug;
use rand::prelude::*;
use rand_distr::Standard;
use rand_distr::OpenClosed01;

use crate::impact::Impact;
use crate::impact::SECONDARY_EJECTA_RATIO;
use crate::particle::Particle;

const SQRT_3: f64 = 1.7320508075688772;

#[derive(Debug)]
pub struct EventOutcome {
    pub depth: f64,
    pub movement: f64,
}

pub trait Event: Debug {
    fn get_outcome(&self, particle: &mut Particle) -> Option<EventOutcome>;
}

#[derive(Debug)]
pub struct BurialEvent {
    pub depth: f64,
}

impl Event for BurialEvent {
    fn get_outcome(&self, particle: &mut Particle) -> Option<EventOutcome> {
        Some(EventOutcome {
            depth: particle.get_current_depth() + self.depth,
            movement: 0.0,
        })
    }
}

#[derive(Debug)]
pub struct ExcavationEvent {
    pub depth: f64,
}

impl Event for ExcavationEvent {
    fn get_outcome(&self, particle: &mut Particle) -> Option<EventOutcome> {
        let new_depth = particle.get_current_depth() - self.depth;
        assert!(new_depth >= 0., "cannot excavate surface level below a particle's current depth");

        Some(EventOutcome {
            depth: new_depth,
            movement: 0.0,
        })
    }
}

#[derive(Debug)]
pub struct BrecciaInfillEvent {
    pub radius: f64,
    pub thickness: f64,
    pub impact_distance: f64,
}

impl Event for BrecciaInfillEvent {
    fn get_outcome(&self, particle: &mut Particle) -> Option<EventOutcome> {
        let rng = particle.get_rng();
        let mut rnd1: f64;
        let mut rnd2: f64;

        loop {
            rnd1 = rng.sample(Standard);
            rnd2 = rng.sample(Standard);
            
            if rnd2 * SQRT_3 * self.radius < 4. * (rnd1 - rnd1.powi(3)) {
                break
            }
        }

        let loc = rnd1 * self.radius;
        let depth = (1. - rnd1.powi(2)) * self.thickness;

        Some(EventOutcome {
            depth,
            movement: loc - self.impact_distance,
        })
    }
}

#[derive(Debug)]
pub struct EjectaBlanketEvent {
    pub impact: Impact,
}

impl Event for EjectaBlanketEvent {
    fn get_outcome(&self, particle: &mut Particle) -> Option<EventOutcome> {
        let rng = particle.get_rng();
        let rnd1: f64 = rng.sample(OpenClosed01);
        let rnd2: f64 = rng.sample(Standard);

        let minimum_distance = self.impact.distance.max(self.impact.diameter / 2.);
        let distance = minimum_distance / rnd1;

        let depth = rnd2 * self.impact.get_ejecta_thickness_at_distance(distance) * (SECONDARY_EJECTA_RATIO + 1.);
        let movement = distance - self.impact.distance;

        Some(EventOutcome { depth, movement })
    }
}

#[derive(Debug)]
pub struct DestructionEvent {
}

impl Event for DestructionEvent {
    #[allow(unused_variables)]
    fn get_outcome(&self, particle: &mut Particle) -> Option<EventOutcome> {
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand_pcg::Pcg64;

    macro_rules! assert_approx_eq {
        ($x:expr, $y:expr) => {
            if $x + $y >= f64::MIN && ($x - $y) / ($x + $y).min(f64::MAX) > f64::EPSILON { panic!(); }
        }
    }

    #[test]
    fn burial_increases_depth() {
        let mut particle = Particle::new(0.001, -4.5, Pcg64::seed_from_u64(0x0));
        particle.apply_event(Box::new(BurialEvent{depth: 0.02}), 0.);
        assert_approx_eq!(particle.get_current_depth(), 0.021);
    }

    #[test]
    fn excavation_reduces_depth() {
        let mut particle = Particle::new(0.001, -4.5, Pcg64::seed_from_u64(0x0));
        particle.apply_event(Box::new(ExcavationEvent{depth: 0.0005}), 0.);
        assert_approx_eq!(particle.get_current_depth(), 0.0005);
    }

    #[test]
    #[should_panic]
    fn cant_excavate_more_than_depth() {
        let mut particle = Particle::new(0.001, -4.5, Pcg64::seed_from_u64(0x0));
        particle.apply_event(Box::new(ExcavationEvent{depth: 1000.0}), 0.);
    }

    // TODO: test breccia infill maths

    #[test]
    fn position_in_ejecta_blanket_is_deterministic() {
        let mut particle = Particle::new(0.001, -4.5, Pcg64::seed_from_u64(0x0));
        let impact = Impact::new(200.0, 50.0, -4.5);
        
        particle.apply_event(Box::new(EjectaBlanketEvent{impact}), 0.);

        assert_approx_eq!(particle.depth, 2.088042773650822);
        assert_approx_eq!(particle.movement[0], 733.3473782148099);
    }

    #[test]
    fn ejected_particle_within_ejecta_blanket() {
        let mut particle = Particle::new(0.001, -4.5, Pcg64::seed_from_u64(thread_rng().next_u64()));
        let impact = Impact::new(200.0, 50.0, -4.5);
        
        particle.apply_event(Box::new(EjectaBlanketEvent{impact}), 0.);
        let new_distance_from_centre = particle.movement[0] + impact.distance;

        assert!(new_distance_from_centre >= impact.diameter / 2.);
        assert!(particle.depth <= 1.1 * impact.get_ejecta_thickness_at_distance(new_distance_from_centre));
    }
}
