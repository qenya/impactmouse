mod neon_quantity;
mod hohenberg_1978;

pub use neon_quantity::NeonQuantity;
pub use hohenberg_1978::*;

// g/cm^3; from Kiefer et al. (2012)
pub const DENSITY_MARE_BASALT: f64 = 3.140;
pub const DENSITY_HIGHLANDS: f64 = 2.400;
