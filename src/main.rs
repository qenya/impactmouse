use rand::prelude::*;
use impactmouse::simulation::Simulation;
use impactmouse::simulation::SimulationProperties;
use impactmouse::distributions::BoundedPareto;
use impactmouse::distributions::Hartmann1999;
use impactmouse::cosmogenesis::*;

#[allow(dead_code)]
const HARTMANN_1964: BoundedPareto = BoundedPareto {
    min_diameter: 1.0,
    max_diameter: 256.0,
    shape: 2.1,
};

const HARTMANN_1999: Hartmann1999 = Hartmann1999 {
    max_diameter: 256.0,
};

fn main() {
    //test_gradual_burial_rate();

    //header for tally_neon();
    //println!("RNG seed,Ne20 (cm^3/g STP),Ne21 (cm^3/g STP),Ne22 (cm^3/g STP),apparent CRE depth (cm),apparent CRE age (Ga),ultimate depth (km)");
    
    for _ in 0..1_000 {
        let sim = Simulation::new(SimulationProperties{
            impact_size_distr: HARTMANN_1999,
            planet_radius: 1737.103,
            time_range: -1.0..0.0,
        });

        println!("SEED={:#}", sim.get_seed());
        print_depth_history(sim, 0.0, 1);
        //tally_neon(sim);
    }
}

#[allow(dead_code)]
fn test_gradual_burial_rate() {
    let sim = Simulation::new(SimulationProperties{
        impact_size_distr: HARTMANN_1999,
        planet_radius: 1737.103,
        time_range: 0.0..100_000.0,
    });

    println!("SEED={:#}", sim.get_seed());
    print_depth_history(sim, 1.0, 10_000);
}


// "trimming" slims down the number of data points, to save time plotting the graph if there are many points
// e.g., for trimming = 1000, only every thousandth point is plotted
#[allow(dead_code)]
fn print_depth_history<T: Distribution<f64>>(mut sim: Simulation<T>, start_depth: f64, trimming: u16) {
    let mut particle = sim.new_particle(start_depth);
    let mut counter: u16 = 0;
    
    while let Some(impact) = sim.next_impact() {
        let event = particle.get_event_from_impact(impact);
        particle.apply_event(event, impact.time);

        if particle.is_destroyed {
            println!("particle destroyed");
            break;
        }

        counter += 1;
        if counter == trimming {
            println!("{:#},{:#}", particle.depth, particle.time);
            counter = 0;
        }
    }
}

#[allow(dead_code)]
fn tally_neon<T: Distribution<f64>>(mut sim: Simulation<T>) {
    let mut particle = sim.new_particle(0.);
    let mut neon = NeonQuantity::ZERO;

    while let Some(impact) = sim.next_impact() {
        let neon_produced_since_last_impact = get_neon_production(particle.depth, DENSITY_MARE_BASALT, impact.time - particle.time);
        neon = neon + neon_produced_since_last_impact;

        //println!("{:#},{:#},{:#},{:#},{:#}", particle.depth, impact.time - particle.time, neon.a20, neon.a21, neon.a22);

        let event = particle.get_event_from_impact(impact);
        particle.apply_event(event, impact.time);

        if particle.is_destroyed {
            println!("particle destroyed");
            return;
        }
    }

    let neon_produced_since_last_impact = get_neon_production(particle.depth, DENSITY_MARE_BASALT, 0. - particle.time);
    neon = neon + neon_produced_since_last_impact;

    let (shielding, age) = calculate_cre_depth_and_age(neon);
    let neon_volume = convert_to_stp_volume(neon);
    println!("{:#},{:#},{:#},{:#},{:#},{:#}", neon_volume.a20, neon_volume.a21, neon_volume.a22, shielding / DENSITY_MARE_BASALT, age, particle.depth);
}

const MINUTES_PER_GYR: f64 = 525948770000000.;
const AVOGADRO_NUMBER: f64 = 6.0221415E+023;
const IDEAL_GAS_VOLUME: f64 = 22400.; // cm^3 per mole at STP
const MASS_FRACTION: f64 = 0.2; // % of rock composed of Mg

fn calculate_cre_depth_and_age(neon: NeonQuantity) -> (f64, f64) {
    let neon_ratio = neon.a22 / neon.a21;
    let shielding = (neon_ratio / 1.448).powf(-1./0.11);
    let prod_rate = get_neon_production_rate(shielding).a22;
    let age = neon.a22 / (prod_rate * MINUTES_PER_GYR as f64);

    (shielding, age)
}

// Input: Ne atoms per minute per kg of Mg
// Output: cm^3 per g of rock at STP
fn convert_to_stp_volume(neon: NeonQuantity) -> NeonQuantity {
    (neon / 1000.) * IDEAL_GAS_VOLUME * MASS_FRACTION / AVOGADRO_NUMBER
}
