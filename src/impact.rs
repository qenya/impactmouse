#[derive(Copy, Clone, Debug)]
pub struct Impact {
    pub diameter: f64,
    pub distance: f64,
    pub time: f64,
    transient_depth: f64,
    apparent_depth: f64,
    observed_depth: f64,
}

// Depth below the surface that material outside the crater is excavated and
// incorporated in the ejecta blanket, as a proportion of the ejecta blanket's
// thickness at that point
// TODO: come up with a better estimate with a physical basis
pub const SECONDARY_EJECTA_RATIO: f64 = 0.1;

impl Impact {
    pub fn new(diameter: f64, distance: f64, time: f64) -> Impact {
        let transient_depth = diameter.powf(1.02) * 0.28;
        let apparent_depth = diameter.powf(1.06) * 0.13;
        let observed_depth = diameter.powf(1.02) * (325./1764.);
        Impact { diameter, distance, time, transient_depth, apparent_depth, observed_depth }
    }

    pub fn get_transient_depth(&self) -> f64 {
        self.transient_depth
    }

    pub fn get_apparent_depth(&self) -> f64 {
        self.apparent_depth
    }

    pub fn get_excavation_depth(&self) -> f64 {
        self.transient_depth / 3.
    }

    pub fn get_transient_diameter(&self) -> f64 {
        self.transient_depth * 3.
    }

    pub fn get_lip_height(&self) -> f64 {
        //self.diameter * 0.036 // Holsapple (2007)
        //self.diameter.powf(1.06) * 0.032928
        self.observed_depth - self.apparent_depth
    }

    pub fn get_ejecta_thickness_at_distance(&self, distance: f64) -> f64 {
        // ("distance" is the distance from the crater center)
        let twice_distance = distance * 2.;
        assert!(twice_distance >= self.diameter, "ejecta blanket thickness is undefined within crater rim");
        (twice_distance / self.diameter).powi(-3) * self.get_lip_height() // Melosh (2011)
    }
}
