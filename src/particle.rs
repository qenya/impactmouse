use rand_pcg::Pcg64;

use crate::event::Event;
use crate::event::BurialEvent;
use crate::event::ExcavationEvent;
use crate::event::BrecciaInfillEvent;
use crate::event::EjectaBlanketEvent;
use crate::event::DestructionEvent;
use crate::impact::Impact;
use crate::impact::SECONDARY_EJECTA_RATIO;

pub struct Particle {
    pub time: f64,          // current time (Gyr from present)
    pub depth: f64,         // current depth (km)
    pub movement: Vec<f64>, // distance (km) of each lateral movement to date
    pub is_destroyed: bool,
    rng: Pcg64,
}

impl Particle {
    pub fn new(start_depth: f64, start_time: f64, rng: Pcg64) -> Particle {
        Particle {
            time: start_time,
            depth: start_depth,
            movement: vec![],
            is_destroyed: false,
            rng,
        }
    }

    pub fn get_event_from_impact(&self, impact: Impact) -> Box<dyn Event> {
        let current_depth = self.get_current_depth();

        let rim_radius = impact.diameter / 2.;
        let transient_radius = impact.get_transient_diameter() / 2.;

        if impact.distance < transient_radius {
            // Epicentre of point is within transient crater
            let transient_depth_at_point = get_paraboloid_depth(transient_radius, impact.distance, impact.get_transient_depth());
            
            if current_depth >= transient_depth_at_point {
                // Point is beneath transient crater
                // Surface is excavated, i.e., particle becomes closer to surface
                let excavation_depth = get_paraboloid_depth(transient_radius, impact.distance, impact.get_apparent_depth());
                Box::new(ExcavationEvent{depth: excavation_depth})
            } else if current_depth >= impact.get_excavation_depth() {
                // Point is within transient crater
                // Particle becomes part of the breccia filling the crater floor
                let transient_depth = impact.get_transient_depth();
                let apparent_depth = impact.get_apparent_depth();
                let max_infill_thickness = transient_depth - apparent_depth;
                
                Box::new(BrecciaInfillEvent{
                    radius: transient_radius,
                    thickness: max_infill_thickness,
                    impact_distance: impact.distance,
                })
            } else if impact.distance * current_depth < (transient_radius * 0.1).powi(2) {
                // Point is close enough to the impactor that the particle is melted or vaporised
                // TODO: Figure out exactly how close it needs to be, rather than hardcoding to 10% the radius
                Box::new(DestructionEvent{})
            } else {
                // Particle is ejected from the crater
                Box::new(EjectaBlanketEvent{impact})
            }
        } else {
            // Epicentre of point is outside the transient crater
            if impact.distance < rim_radius {
                // Epicentre of point is nevertheless within the crater rim (this is unlikely but possible)
                // Particle is buried by the outermost part of the crater floor
                let surface_depression_below_rim = -get_paraboloid_depth(transient_radius, impact.distance, impact.get_apparent_depth());
                Box::new(BurialEvent{depth: impact.get_lip_height() - surface_depression_below_rim})
            } else {
                // Epicentre of point is outside the crater rim
                if current_depth < impact.get_ejecta_thickness_at_distance(impact.distance) * SECONDARY_EJECTA_RATIO {
                    // Particle is close enough to the surface to be caught up in the ejecta blanket
                    Box::new(EjectaBlanketEvent{impact})
                } else {
                    // Particle is buried by the ejecta blanket, if it reaches this far
                    Box::new(BurialEvent{depth: impact.get_ejecta_thickness_at_distance(impact.distance)})
                }
            }
        }
    }

    pub fn get_current_depth(&self) -> f64 {
        self.depth
    }

    pub fn get_current_time(&self) -> f64 {
        self.time
    }

    pub fn apply_event(&mut self, event: Box<dyn Event>, time: f64) {
        assert!(time >= self.get_current_time(), "cannot alter a particle's history after the fact");
        assert!(!self.is_destroyed, "cannot apply an event to a destroyed particle");
        let event_outcome = event.get_outcome(self);
        self.time = time;

        if let Some(outcome) = event_outcome {
            self.depth = outcome.depth;
            self.movement.push(outcome.movement);
        } else {
            self.is_destroyed = true;
        }
    }

    pub fn get_rng(&mut self) -> &mut Pcg64 {
        &mut self.rng
    }
}

fn get_paraboloid_depth(radius: f64, distance_from_centre: f64, central_depth: f64) -> f64 {
    let depth_factor = 1.0 - (distance_from_centre / radius).powi(2);
    central_depth * depth_factor
}
