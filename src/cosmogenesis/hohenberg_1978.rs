use crate::cosmogenesis::NeonQuantity;

const MINUTES_PER_GYR: u64 = 525948770000000;

// shielding: units of g/cm^2
// return value: units of atoms/min/kg
// taken directly from Hohenberg et al. (1978), table 1a
pub fn get_neon_production_rate(shielding: f64) -> NeonQuantity {
    if shielding < 1.0 {
        let proportion = (shielding - 0.0) / (1.0 - 0.0);
        NeonQuantity {
            a20: 5019.0 + (1224.0 - 5019.0) * proportion,
            a21: 1782.0 + ( 590.0 - 1782.0) * proportion,
            a22: 1766.0 + ( 806.0 - 1766.0) * proportion,
        }
    } else if shielding < 2.0 {
        let proportion = (shielding - 1.0) / (2.0 - 1.0);
        NeonQuantity {
            a20: 1224.0 + (760.0 - 1224.0) * proportion,
            a21:  590.0 + (453.0 -  590.0) * proportion,
            a22:  806.0 + (619.0 -  806.0) * proportion,
        }
    } else if shielding < 5.0 {
        let proportion = (shielding - 2.0) / (5.0 - 2.0);
        NeonQuantity {
            a20: 760.0 + (448.0 - 760.0) * proportion,
            a21: 453.0 + (358.6 - 453.0) * proportion,
            a22: 619.0 + (455.0 - 619.0) * proportion,
        }
    } else if shielding < 10.0 {
        let proportion = (shielding - 5.0) / (10.0 - 5.0);
        NeonQuantity {
            a20: 448.0 + (387.7 - 448.0) * proportion,
            a21: 358.6 + (354.8 - 358.6) * proportion,
            a22: 455.0 + (409.2 - 455.0) * proportion,
        }
    } else if shielding < 20.0 {
        let proportion = (shielding - 10.0) / (20.0 - 10.0);
        NeonQuantity {
            a20: 387.7 + (417.9 - 387.7) * proportion,
            a21: 354.8 + (399.6 - 354.8) * proportion,
            a22: 409.2 + (417.0 - 409.2) * proportion,
        }
    } else if shielding < 40.0 {
        let proportion = (shielding - 20.0) / (40.0 - 20.0);
        NeonQuantity {
            a20: 417.9 + (468.0 - 417.9) * proportion,
            a21: 399.6 + (452.0 - 399.6) * proportion,
            a22: 417.0 + (430.0 - 417.0) * proportion,
        }
    } else if shielding < 65.0 {
        let proportion = (shielding - 40.0) / (65.0 - 40.0);
        NeonQuantity {
            a20: 468.0 + (480.0 - 468.0) * proportion,
            a21: 452.0 + (462.0 - 452.0) * proportion,
            a22: 430.0 + (417.0 - 430.0) * proportion,
        }
    } else if shielding < 100.0 {
        let proportion = (shielding - 65.0) / (100.0 - 65.0);
        NeonQuantity {
            a20: 480.0 + (435.0 - 480.0) * proportion,
            a21: 462.0 + (418.0 - 462.0) * proportion,
            a22: 417.0 + (362.0 - 417.0) * proportion,
        }
    } else if shielding < 150.0 {
        let proportion = (shielding - 100.0) / (150.0 - 100.0);
        NeonQuantity {
            a20: 435.0 + (368.0 - 435.0) * proportion,
            a21: 418.0 + (353.0 - 418.0) * proportion,
            a22: 362.0 + (292.0 - 362.0) * proportion,
        }
    } else if shielding < 225.0 {
        let proportion = (shielding - 150.0) / (225.0 - 150.0);
        NeonQuantity {
            a20: 368.0 + (281.0 - 368.0) * proportion,
            a21: 353.0 + (269.0 - 353.0) * proportion,
            a22: 292.0 + (211.0 - 292.0) * proportion,
        }
    } else if shielding < 500.0 {
        let proportion = (shielding - 225.0) / (500.0 - 255.0);
        NeonQuantity {
            a20: 281.0 + (52.6 - 281.0) * proportion,
            a21: 269.0 + (50.1 - 269.0) * proportion,
            a22: 211.0 + (36.7 - 211.0) * proportion,
        }
    } else {
        NeonQuantity::ZERO
    }
}

// depth: units of km
// density (of overlying mass): units of g/cm^3
// time: units of Gyr
// return value: units of atoms/kg
pub fn get_neon_production(depth: f64, density: f64, time: f64) -> NeonQuantity {
    let rate = get_neon_production_rate(depth * density * 100_000.);
    rate * time * MINUTES_PER_GYR as f64
}
