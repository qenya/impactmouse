use std::fmt::Debug;
use std::ops::{Add, Sub, Mul, Div};

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct NeonQuantity {
    pub a20: f64,
    pub a21: f64,
    pub a22: f64,
}

impl NeonQuantity {
    pub const ZERO: Self = Self {
        a20: 0.,
        a21: 0.,
        a22: 0.,
    };
}

impl Add for NeonQuantity {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            a20: self.a20 + other.a20,
            a21: self.a21 + other.a21,
            a22: self.a22 + other.a22,
        }
    }
}

impl Sub for NeonQuantity {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self {
            a20: self.a20 - other.a20,
            a21: self.a21 - other.a21,
            a22: self.a22 - other.a22,
        }
    }
}

impl Mul<f64> for NeonQuantity {
    type Output = Self;

    fn mul(self, other: f64) -> Self {
        Self {
            a20: self.a20 * other,
            a21: self.a21 * other,
            a22: self.a22 * other,
        }
    }
}

impl Div<f64> for NeonQuantity {
    type Output = Self;

    fn div(self, other: f64) -> Self {
        Self {
            a20: self.a20 / other,
            a21: self.a21 / other,
            a22: self.a22 / other,
        }
    }
}
