use rand::prelude::*;
use rand_distr::Pareto;

pub struct BoundedPareto {
    pub min_diameter: f64, // km
    pub max_diameter: f64, // km
    pub shape: f64,
}

impl Distribution<f64> for BoundedPareto {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> f64 {
        let distr = Pareto::new(self.min_diameter, self.shape).unwrap();

        let mut diameter;
        loop {
            diameter = rng.sample(&distr);
            if diameter < self.max_diameter {
                break;
            }
        }

        diameter
    }
}
