use rand::prelude::*;
use rand_distr::OpenClosed01;

pub struct Hartmann1999 {
    pub max_diameter: f64, // km
}

const INV_NEG_SHAPE_1: f64 = -1. / 3.82;
const INV_NEG_SHAPE_2: f64 = -1. / 1.80;
const INV_NEG_SHAPE_3: f64 = -1. / 2.20;

impl Distribution<f64> for Hartmann1999 {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> f64 {
        let mut diameter;

        loop {
            let rnd: f64 = rng.sample(OpenClosed01);

            // This is here to make the minimum crater diameter
            // 1 km (as required by the chronology function I'm using)
            // rather than 200 m (as this production function is designed for)
            let rnd = rnd * 0.002410;

            diameter = if rnd > 0.000644331 { // d < 1.4 km
                rnd.powf(INV_NEG_SHAPE_1) * 0.206625
            } else if rnd > 0.000000677642 { // d < 64 km
                rnd.powf(INV_NEG_SHAPE_2) * 0.0238659
            } else {
                rnd.powf(INV_NEG_SHAPE_3) * 0.10021
            };

            if diameter < self.max_diameter {
                break;
            }
        }

        diameter
    }
}
