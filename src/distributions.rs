mod bounded_pareto;
mod hartmann_1999;

pub use bounded_pareto::BoundedPareto;
pub use hartmann_1999::Hartmann1999;

#[cfg(test)]
mod tests {
    use super::*;
    use rand::prelude::*;
    
    #[test]
    #[ignore]
    fn sample_bounded_pareto() {
        let distr = BoundedPareto {
            min_diameter: 1.0,
            max_diameter: 256.0,
            shape: 2.1,
        };

        let mut rng = thread_rng();

        for _ in 0..1_000_000 {
            println!("{:#}",rng.sample(&distr));
        }
    }

    #[test]
    #[ignore]
    fn sample_hartmann_1999() {
        let distr = Hartmann1999 { max_diameter: 256.0 };

        let mut rng = thread_rng();

        for _ in 0..1_000_000 {
            println!("{:#}",rng.sample(&distr));
        }
    }
}
