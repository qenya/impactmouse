use std::f64::consts;
use std::ops::Range;
use rand::prelude::*;
use rand_distr::Exp;
use rand_distr::Open01;
use rand_pcg::Pcg64;

use crate::impact::Impact;
use crate::particle::Particle;

const PCG64_DEFAULT_STREAM: u128 = 0xa02bdbf7bb3c0a7ac28fa16a64abf96;

pub const FULL_LUNAR_AGE_RANGE: Range<f64> = -4.5..0.; // Gyr

pub struct SimulationProperties<T: Distribution<f64>> {
    //pub impact_distr_shape: f64,
    //pub impact_min_diameter: f64, // km
    pub impact_size_distr: T,
    pub planet_radius: f64, // km
    pub time_range: Range<f64>, // Gyr
}

pub struct Simulation<T: Distribution<f64>> {
    impact_size_distr: T,
    planet_radius: f64,
    planet_area: f64,
    rng: Pcg64,
    rng_seed: u64,
    next_rng_stream: u128,
    current_age: f64,
    max_age: f64,
}

impl<T> Simulation<T> where T: Distribution<f64> {
    pub fn new(props: SimulationProperties<T>) -> Simulation<T> {
        Simulation::<T>::new_seeded(props, thread_rng().next_u64())
    }
    
    pub fn new_seeded(props: SimulationProperties<T>, rng_seed: u64) -> Simulation<T> {
        //assert!(props.impact_distr_shape > 0.0);
        //assert!(props.impact_min_diameter > 0.0);
        assert!(props.planet_radius > 0.0);

        //let impact_size_distr = Pareto::new(props.impact_min_diameter, props.impact_distr_shape).unwrap();
        let rng = Pcg64::new(rng_seed.into(), PCG64_DEFAULT_STREAM);

        Simulation {
            impact_size_distr: props.impact_size_distr,
            planet_radius: props.planet_radius,
            planet_area: props.planet_radius.powi(2) * consts::PI,
            rng,
            rng_seed,
            next_rng_stream: PCG64_DEFAULT_STREAM + 1,
            current_age: props.time_range.start,
            max_age: props.time_range.end,
        }
    }

    pub fn next_impact(&mut self) -> Option<Impact> {
        let diameter = self.rng.sample(&self.impact_size_distr);
        if diameter < 0.0 {
            panic!("impact generated with a negative diameter - check that the probability distribution is correct!")
        }
        
        let rnd: f64 = self.rng.sample(Open01);
        let distance = self.planet_radius * (rnd * 2.0 - 1.0).acos();

        let residence_time = Exp::new(self.get_cratering_rate()).unwrap().sample(&mut self.rng);
        self.current_age += residence_time;

        if self.current_age > self.max_age {
            Option::None
        } else {
            Some(Impact::new(diameter, distance, self.current_age))
        }
    }

    pub fn new_particle(&mut self, start_depth: f64) -> Particle {
        let particle_rng = Pcg64::new(self.rng_seed.into(), self.next_rng_stream);
        self.next_rng_stream += 1;

        Particle::new(start_depth, self.current_age, particle_rng)
    }

    fn get_cratering_rate(&self) -> f64 {
        let rate_per_sq_km = 0.000000000000376992 * consts::E.powf(-self.current_age * 6.93) + 0.000838; // Neukum et al. (2001)
        //let rate_per_sq_km = 0.000838;
        rate_per_sq_km * self.planet_area
    }

    pub fn get_seed(&self) -> u64 {
        self.rng_seed
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::distributions::BoundedPareto;

    #[test]
    #[should_panic]
    fn invalid_max_crater_distance() {
        Simulation::new(SimulationProperties{
            impact_size_distr: BoundedPareto {
                min_diameter: 1.0,
                max_diameter: 64.0,
                shape: 2.7,
            },
            planet_radius: -2.0, // Must be positive
            time_range: FULL_LUNAR_AGE_RANGE,
        });
    }

    #[test]
    #[ignore]
    fn sample_impact_distribution() {
        let mut sim = Simulation::new(SimulationProperties{
            impact_size_distr: BoundedPareto {
                min_diameter: 1.41,
                max_diameter: 64.0,
                shape: 2.7,
            },
            planet_radius: 1737.103,
            time_range: FULL_LUNAR_AGE_RANGE,
        });

        // get up to a million results...
        for _ in 0..1_000_000 {
            if let Some(impact) = sim.next_impact() {
                println!("{},{}", impact.diameter, impact.distance);
            } else {
                // but stop early if the simulation runs out of time.
                break;
            }
        }
    }
}
