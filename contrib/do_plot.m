#!/usr/bin/env -S octave-cli -qf

# usage: ./do_plot.m path/to/input_file.csv
# format of CSV should be depth,time with no column headers
# outputs to path/to/input_file.png

clear all;

if nargin < 1
  disp("missing filename argument");
  exit(1);
end

file_in = argv(){1};
idx = rindex(file_in, ".");
if (idx > 0)
  file_out = [substr(file_in,1,idx-1) ".png"];
else
  file_out = [file_in ".png"];
endif

data = load("-ascii", file_in);

figure(1); clf;
plot(data(:,2), data(:,1));
set(gca(), "ydir", "reverse");
xlabel("time / Gyr");
ylabel("depth / km");

print(1, file_out, '-dpng');
